<?php
/**
	* @file Class to calculate and manipulate the dimension input.
	*/
class calculator {
	
	/**
	 * Defines constants for conversion formulas.
	 */
  const METERS_IN_INCHES = 0.00064516;
  const INCHES_IN_FEET2 = 144;
	const INCHES_IN_FEET = 12;
	const INCHES_IN_YD = 36;
	const INCHES_IN_METER = 39.3701;
  const INCHES_IN_CENT = 0.3937;
	const DEFAULT_OVERAGE = .3;
	
	function __construct() {
		$this->overage = static::DEFAULT_OVERAGE;
	  $this->walls = array();
	  $this->total = 0;
	  $this->total_metric = 0;
		$this->context = array();
	}
	
	/**
	 * Calcuates the sum of all walls area's.
	 *
	 * @param string $units either 'metric' or default NULL for feet.
	 * 
	 * @return float either total in sq meters or sq feet.
	 */
	public function get_grand_total($units = NULL) {
		if($this->total || $this->total_metric) {
			if ($units == 'metric') {
				return ($this->total_metric + ($this->total_metric * $this->overage));
			}
				return ($this->total + ($this->total * $this->overage));
		}
	}
	
	/** 
	 * Parses a human readable string of dimensions ie. 5' 3 inches or 72 m 16 cm etc.
	 *
	 * @param $string the string to be parsed.
   * @param $num array stores the output and may contain starting values ie. $num['feet'] = 3, will be added to any other feet values.
	 * @param $reg array accepts an alternate set set of regex to check against.
	 * 
	 * @return array containing keys for length and width, or FALSE if none are found.
	 */
	public function validate_measurments($string, $num = array(), $reg) {

		//default regex to match.
		if(!is_array($reg)) {
			$reg = array();
			$reg['feet'] = '/(\d\.?\,?)+(?=\s*(?=f|F|\'))/';
			$reg['inches'] = '/(\d\.?\,?)+(?=\s*(I|i|\'\'|"))/';
			$reg['yards'] = "/(\d\.?\,?)+(?=\s*(y|Y))/";
			$reg['meters'] = "/(\d\.?\,?)+(?=\s*(M|m))/";
			$reg['centimeters'] = "/(\d\.?\,?)+(?=\s*(c|C))/";
		}
		
		//loop through reg array and match input string.
		foreach( $reg as $key=>$value ) {
			if (preg_match($value, $string, $matches) !== 0) {
				//remove commas from first match.
				$matches[0] = (float) str_replace(',','',$matches[0]);
				//accumulate like units.
				$num[$key] += $matches[0];
				//replace the current match.
				$newstring = preg_replace($value,'', $string, 1);	
				//call this function recursivly on the new string.
				return $this->validate_measurments($newstring, $num, $reg);
			}
		}
		
		//Checks if num is not empty.
		if(!sizeof($num)){
			return "invalid format";
		} 
		
		return $num;
		
	}//validate_measurments method.

  /**
	 * This function takes validated measurements and converts them to square inches.
	 *
	 * @param walls an array of walls each containing a height and width.
	 *
	 * @return an the computed area of the wall (feet and meters) [total] of all walls in square inches.
 	 */
	public function compute_walls($walls) {
		
		if(is_array($walls)) {
			
			foreach($walls as $value) {
				
				// Converts width and heigt to inches and gets area (square inches).
				$h = $this->_to_inches($value['height']);
				$w = $this->_to_inches($value['width']);
				$sqin = ($h * $w);
				
				// Saves wall area in both square feet and square meters.	
				$wall[] = array( 
					'feet' => $this->get_formats($sqin), 
					'meters' => $this->get_formats($sqin, 'metric'), 
				);
				
				// Stores runnning total walls in square inches.
				$wall['total'] += $sqin;
			}
		} 

		$meters = $this->get_formats($wall['total'], 'metric');
		$feet = $this->get_formats($wall['total']);
		$this->total += $feet[0];
		$this->total_metric += $meters[0];
		
		$this->walls[] = $wall;
		
		return $wall;
	}//compute_walls method

	/**
	 * Converts square inches to either meters or feet.
	 *
	 * @param float|int $sqin the number of inches.
	 * @param string $op either 'metric' or NULL.
	 *
	 * @return an array with the value [0] and units label [1].
	 */
	protected function get_formats($sqin, $op) {
		$num = '';
		$label = '';
		$output = array();
		
		if ($op == 'metric') {
			$num = ($sqin * static::METERS_IN_INCHES);
			$output[0] = $num;
			$output[1] = 'square meters';
		} 
		else {
			$num = ($sqin / static::INCHES_IN_FEET2);
			$output[0] = $num;
			$output[1] = 'square feet';
		}
			return $output;
	}//get_formats method

 /** 
	* Takes an array from the validate_measurments method and convertes it to inches.
	*/
	protected function _to_inches($dims = array()) {
		$inches = 0;	
		foreach($dims as $key => $value) {
			if(is_array($value)) {
				$this->_to_inches($value);
			}
			switch($key) {
				case 'feet':
					$inches += ($value * static::INCHES_IN_FEET);
					break;
				case 'inches':
					$inches += $value;
					break;
				case 'yards':
					$inches += ($value * static::INCHES_IN_YD);
					break;
				case 'meters':
					$inches += ($value * static::INCHES_IN_METER);
					break;
				case 'centimeters':
					$inches += ($value * static::INCHES_IN_CENT);
				 break;
			}
		}
		return $inches;
	}//_to_inches method.

	/**
	 * Defines setters.
	 */
	public function set_overage($percent) {
		$this->overage = $percent;
	}
	protected function set_context($context = array()) {
		$this->context = $context;
	}

	/**
		* Defines getters.
		*/
	public function get_overage() {
		return $this->overage;
	}
	public function get_context() {
		return $this->context;
	}
	protected function get_total_inches() {
		return $this->walls[0]['total'];
	}

	/**
	 * Defines instance variables.
	 */
		private $walls;
		private $total;
		private $total_metric;
		private $context;
		private $overage;
	
}//calculator class.

/**
 * A class that adds product specific calculation to the calculator class.
 */	
Class productCalculator extends calculator {
	
	// Sets the number of square inches to use if the product is of custom type.
	const CUSTOM_SQIN = 4320;
	
	function __construct($node) {
		parent::__construct();
		$this->set_context($this->_calc_context($node));	
	}

	/**
	 * This function gets dimensions and other values for the current product.
	 *
	 * @param stdClass node A node object.
	 *
	 * @return array context if a context can be determined for the product node.
	 * @return false if no context can be determined.
	 */ 
	function _calc_context($node) {
		
		//makes sure the node is a product.
		if(in_array($node->type, module_invoke_all('product_types'))) {

			// Set variables from appopriate fields holding corresponding dimensions.
			// @todo make this a configuration where admins select which fields to use.
			$width = $node->field_roll_width[0]['value'];
			$lengthf = $node->field_roll_length_feet[0]['value'];
			$lengthy = $node->field_roll_length_yards[0]['value'];

			// Checks if both width and height field are available from desiginated fields.
			// If found convert them to inches.
			if($lengthf && $width || $lengthy && $width) {

				if($lengthf > 0) {
					$length = $lengthf * static::INCHES_IN_FEET;
				}
				else if($lengthy > 0) {
					$length =  $lengthy * static::INCHES_IN_YD;
				}
			}
			else {
				// if no dimension fields exist we attempt to strip them from the node body.
				$newdims = $this->validate_measurments($node->body);
				
				if ($newdims['inches'] && $newdims['feet'] || $newdims['yards']) {
					// Assumes inches are width.
					$width = $newdims['inches'];
					// Assumes yards and feet are length.
					$length = ($newdims['feet'] * static::INCHES_IN_FEET) + ($newdims['yards'] * static::INCHES_IN_YD);
				}

			}
      // If demensions are found then calculate area.
			if($length && $width) {
				$sqin = $length * $width;
			}
			// If not check if it is a custom print to order product.
			else if($node->field_repeat_width[0]['value'] || $node->field_repeat_height[0]['value'] || $node->is_custom[0]['value']) {
				$sqin = static::CUSTOM_SQIN;
			}
			// builds context array to returm;
			$context = array( 
			 'title' => $node->title,
			 'sku' => $node->model,
			 'width' => $width,
			 'length' => $length,
			 'sqft' => $sqin / static::INCHES_IN_FEET2,
			 'sqin' => $sqin,
			 'length_y' => $node->field_roll_length_yards[0]['value'],
			 'length_f' =>$node->field_roll_length_feet[0]['value'],
			 'sold_by' => $node->field_sold_by[0]['value'],
			 'match_type' => $node->field_match_type[0]['value'],
			 'repeat width' => $node->field_repeat_width[0]['value'],
			 'repeat height' => $node->field_repeat_height[0]['value'],
			);
		} 
		
		if (sizeof($context)) {
			return $context;
		}		
			return FALSE;
	}

	/**
	 * Returns the suggested product quantity to order.
	 */
	public function suggest_quantity() {
		$subtotal = $this->get_total_inches();
		$context = $this->get_context();
	  $overage = $this->get_overage() * $subtotal;
		$total =  ($subtotal + $overage);
		
		if($context['sqin'] && $total) {
				$output = round($total / $context['sqin']);
				if($output == 0) {
					$output = 1;
				}
				return $output;
		}
		return 'Cannot suggest quantity for this product';	
	}

	/**
	 * @return string A summary of the Class usage.
	 */
	public function to_string() {
		$subtotal = $this->get_total_inches();
		$context = $this->get_context();
	
		$output = "suggested quantity of " . $context['title'] . " is " . $this->suggestQuantity() . " based on total surface area of " . $subtotal . " with " . $this->get_overage() ."% overage.";
	}

}


// JavaScript Document
if(Drupal.jsEnabled) {
	(function($) {
		$(function() {
			
			// Stores calculator block in variable.
			var calculatorBlock = $('#block-wallcovering_calculator-prod_calc');
			
			// Finds center of the page to center calculator.
			var offset_x = Math.round( ( $('#page').width() / 2 ) - ( calculatorBlock.width() / 2 ) ) + "px";
			var offset_y = Math.round( ( $(window).height() / 2 ) - ( calculatorBlock.height() / 2 ) ) + "px";
			
			//  Adds close button functionality.
			calculatorBlock.css({"position":"absolute","left": offset_x,"top": offset_y, 'z-index' : 10000});
			$('#block-wallcovering_calculator-prod_calc #close').click(function() {
				calculatorBlock.hide();
			});
			
			$('#calculator-link a').click(function(){
				calculatorBlock.toggle();
				return false;
			});
			// Starts drag on calc window.
			// @see dom-drag.js.
			Drag.init(document.getElementById("handle"), document.getElementById("block-wallcovering_calculator-prod_calc"));
			
			// use ajax to get the help data.
			// display it as an overlay on the calc block.
			var helpButton = $("#calc-help-wrapper a, a.calc-toggle");
			
			// asynchronously loads help text from file.
			// @see calculator-help.tpl.php.
			$.get('/calcwalls/js/instructions', 
				function(data){
					calculatorBlock.append(data);
				}
			);
			
			//  adds click behavior to help link and collapse arrow.
			helpButton.click(function(){
					$("#calc-help").toggle();
					$('a.calc-toggle').toggleClass('dwn-arrow');
					return false;
			});
		
			
		});// document ready
	})( jQuery );// jqery scoping
}// drupal closure
<?php
/**
 * @file This is a template file for the calculator instructions.
 */
?>
<div id = 'calc-help'>
	<h2>How to use the calculator</h2>
	<dl>
		<dt>
			1. Adjust your desired overage.
		</dt>
		<dd>
			Overage is the percentage of extra area you will need to account for lost material due to repeat matching, irregular spaces around doors/ windows, etc. We recommend 30% for most wallpaper patterns with straight or offset matches. If you are doing a wall with no windows or doors you may want to change this to a lower percentage. This does not apply to murals since they are a standard size or constrained to the artwork's proportions.
		</dd>
		<dt>
			2. Enter width and height.
		</dt>
		<dd>
			You can enter dimensions in many formats and the calculator will figure it out (ex. 12 feet 3 in, 10 meters, 10' 5", 30 yds). You must enter the units after the number.
		</dd>
		<dt>
			3. Add another wall.
		</dt>
		<dd>
			After you add dimensions for a wall you can click '+ wall' to add another. The total square footage will be the sum total of each wall.
		</dd>
		<dt>
			4. Calculate.
		</dt>
		<dd>
			The calculator will tell you how many units of the current product you will need for most products. If the dimensions are unavailable to the calculator it will return the total square feet you are trying to cover. Please observe the product details and verify dimensions there may be non-standard products that return inaccurate purchasing suggestions. 99% of the time they are correct but we are not responsible for orders based on inaccurate measurements. 
		</dd>
	</dl>
</div>